package com.example.android.miniexercise1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class picture3 extends AppCompatActivity implements View.OnClickListener {
    private Button back3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture3);

        back3 = (Button) findViewById(R.id.back3);
        back3.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == back3) {
            Intent main = new Intent(this, MainActivity.class);
            finish();
            startActivity(main);

        }
    }
}
