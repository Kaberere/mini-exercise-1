package com.example.android.miniexercise1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

private Button picture1,picture2,picture3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        picture1=(Button)findViewById(R.id.picture1);
        picture2=(Button)findViewById(R.id.picture2);
        picture3=(Button)findViewById(R.id.picture3);

        picture1.setOnClickListener(this);
        picture2.setOnClickListener(this);
        picture3.setOnClickListener(this);

    }
    @Override
    public void onClick(View v){
        if(v==picture1){
            Intent pic1=new Intent(this,picture1.class);
            finish();
            startActivity(pic1);
        }

        if(v==picture2){
            Intent pic2=new Intent(this,picture2.class);
            finish();
            startActivity(pic2);
        }
        if(v==picture3){
            Intent pic3=new Intent(this,picture3.class);
            finish();
            startActivity(pic3);
        }


    }

}
